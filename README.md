## LogSeq ipfs sync

[![npm version](https://badge.fury.io/js/%40logseq%2Flibs.svg)](https://badge.fury.io/js/%40logseq%2Flibs)

### Dev setup

- `pnpm i && pnpm dev` in terminal to install dependencies.
- `Load unpacked plugin` in Logseq Desktop client.

##### [Logseq.Editor](https://logseq.github.io/plugins/interfaces/ieditorproxy.html)

- `insertBlock: (
  srcBlock: BlockIdentity, content: string, opts?: Partial<{ before: boolean; sibling: boolean; properties: {} }>
  ) => Promise<BlockEntity | null>`
- `insertBatchBlock: (
  srcBlock: BlockIdentity, batch: IBatchBlock | Array<IBatchBlock>, opts?: Partial<{ before: boolean, sibling: boolean }>
  ) => Promise<Array<BlockEntity> | null>`
- `updateBlock: (
  srcBlock: BlockIdentity, content: string, opts?: Partial<{ properties: {} }>
  ) => Promise<void>`
- `removeBlock: (
  srcBlock: BlockIdentity
  )`
