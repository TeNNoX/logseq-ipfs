import { BlockEntity, IBatchBlock } from '@logseq/libs/dist/LSPlugin'
// import * as wn from 'webnative'
import { CeramicClient } from '@ceramicnetwork/http-client'
import { EthereumAuthProvider } from '@ceramicnetwork/blockchain-utils-linking'
import { DIDDataStore } from '@glazed/did-datastore'
import { DIDSession } from '@glazed/did-session'
// import AuthClient, { generateNonce } from '@walletconnect/auth-client'
// import QRCodeModal from '@walletconnect/qrcode-modal'
import { create as createIPFS } from 'ipfs-core'
import * as dagJose from 'dag-jose'
import { DID } from 'dids'
import { Ed25519Provider } from 'key-did-provider-ed25519'
import KeyResolver from 'key-did-resolver'
import { randomBytes } from '@stablelib/random'
import { DateTime } from 'luxon'
import { CID } from 'multiformats/cid'
// import { decodeCID } from 'webnative/common'

const ipfs = await createIPFS({
  ipld: { codecs: [/* dagJose */] },
  // offline: true,
  // start: false,
  // preload: {
  //   enabled: true, // preload by default, unless in test env
  //   addresses: [
  //     '/dns4/node0.preload.ipfs.io/wss',
  //     '/dns4/node1.preload.ipfs.io/wss',
  //     '/dns4/node2.preload.ipfs.io/wss',
  //     '/dns4/node3.preload.ipfs.io/wss',
  //   ],
  // },
  libp2p: {
    // addresses: [],
  },
})
const seed = randomBytes(32)

// create did instance
const provider = new Ed25519Provider(seed)
const did = new DID({ provider, resolver: KeyResolver.getResolver() })
await did.authenticate()
// window.did = did
console.log('Connected with DID:', did.id)

// const authClient = await AuthClient.init({
//   projectId: '09a741624d5f4a635c7e8102bb9cd1ab',
//   metadata: {
//     name: 'logseq-ceramic',
//     description: 'LogSeq sync via Ceramic',
//     url: 'logseq-ceramic.zt.ax',
//     icons: ['https://my-auth-dapp.com/icons/logo.png'],
//   },
// })

const ceramic = new CeramicClient('https://ceramic-clay.3boxlabs.com')

const aliases = {
  schemas: {
    basicProfile: 'ceramic://k3y52l7qbv1frxt706gqfzmq6cbqdkptzk8uudaryhlkf6ly9vx21hqu4r6k1jqio',

  },
  definitions: {
    BasicProfile: 'kjzl6cwe1jw145cjbeko9kil8g9bxszjhyde21ob8epxuxkaon1izyqsu8wgcic',
  },
  tiles: {},
}

const ceramicStore = new DIDDataStore({ ceramic, model: aliases })

// authClient.on('auth_response', ({ params }) => {
//   console.info('auth response', params)
//   if (params.result?.s) {
//     // Response contained a valid signature -> user is authenticated.
//   } else {
//     // Handle error or invalid signature case
//     console.error(params.message)
//   }
// })
// const { uri } = await authClient.request({
//   aud: 'logseq-ceramic.localhost/login',
//   domain: 'logseq-ceramic.zt.ax',
//   chainId: 'eip155:1',
//   nonce: generateNonce(),
// })
// if (uri) {
//   QRCodeModal.open(uri, () => {
//     console.log('EVENT', 'QR Code Modal closed')
//   })
// }

async function addSignedObject (payload) {
  // sign the payload as dag-jose
  const { jws, linkedBlock } = await did.createDagJWS(payload)

  // put the JWS into the ipfs dag
  const jwsCid = await ipfs.dag.put(jws, { storeCodec: dagJose.name, hashAlg: 'sha2-256' })

  // put the payload into the ipfs dag
  await ipfs.block.put(linkedBlock, jws.link)

  return jwsCid
}

export async function createIPFSData (): Promise<IBatchBlock[]> {
  // console.log('starting ipfs')
  // await ipfs.start()
  // console.log('started ipfs')

  // Create our first signed object
  const cid1 = await addSignedObject({ content: `test ${DateTime.now().toString()}` })

  // Log the DagJWS:
  console.log('cid1', cid1.toString(), (await ipfs.dag.get(cid1)).value)
  // > {
  // >   payload: "AXESIHhRlyKdyLsRUpRdpY4jSPfiee7e0GzCynNtDoeYWLUB",
  // >   signatures: [{
  // >     signature: "h7bHmTaBGza_QlFRI9LBfgB3Nw0m7hLzwMm4nLvcR3n9sHKRoCrY0soWnDbmuG7jfVgx4rYkjJohDuMNgbTpEQ",
  // >     protected: "eyJraWQiOiJkaWQ6MzpiYWdjcWNlcmFza3hxeng0N2l2b2tqcW9md295dXliMjN0aWFlcGRyYXpxNXJsem4yaHg3a215YWN6d29hP3ZlcnNpb24taWQ9MCNrV01YTU1xazVXc290UW0iLCJhbGciOiJFUzI1NksifQ"
  // >   }],
  // >   link: CID(bafyreidykglsfhoixmivffc5uwhcgshx4j465xwqntbmu43nb2dzqwfvae)
  // > }

  // Log the payload:
  await ipfs.dag.get(cid1, { path: '/link' }).then(b => console.log('cid1/link', b.value))
  // > { hello: 'world' }

  // // Create another signed object that links to the previous one
  // const cid2 = await addSignedObject({ hello: 'getting the hang of this', prev: cid1 })
  // console.log('cid2', (await ipfs.dag.get(cid2)).value)

  // // Log the new payload:
  // await ipfs.dag.get(cid2, { path: '/link' }).then(b => console.log('cid2/link', b.value))
  // // > {
  // // >   hello: 'getting the hang of this'
  // // >   prev: CID(bagcqcerappi42sb4uyrjkhhakqvkiaibkl4pfnwpyt53xkmsbkns4y33ljzq)
  // // > }

  // // Log the old payload:
  // await ipfs.dag.get(cid2, { path: '/link/prev/link' }).then(b => console.log('cid2/prev/link', b.value))
  // // > { hello: 'world' }
  // console.log('sig?', await did.verifyJWS((await ipfs.dag.get(cid2)).value))

  //   logseq.FileStorage.setItem('test', 'testv')
  //   const result = logseq.FileStorage.getItem('test')
  return [{ content: JSON.stringify(null, undefined, 2) }]
  //   const endpoint = 'https://www.reddit.com/r/logseq/hot.json'

  //   const { data: { children } } = await fetch(endpoint).then(res => res.json())
  //   const ret = children || []

  //   return ret.map(({ data }, i) => {
  //     const { title, selftext, url, ups, downs, num_comments } = data

  //     return `${i}. [${title}](${url}) [:small.opacity-50 "🔥 ${ups} 💬 ${num_comments}"]
  // collapsed:: true
  // > ${selftext}`
  //   })
}

export async function loadBlock (cid: string, targetBlock: BlockEntity): Promise<void> {
  const placeholder = await logseq.Editor.insertBlock(targetBlock.uuid, '🚀 Fetching ...', { before: false })
  if (!placeholder) throw new Error('failed to create placeholder')

  const block = await ipfs.dag.get(CID.parse(cid), { path: '/link' })
  console.log('resolved block:', block)

  await logseq.Editor.updateBlock(placeholder.uuid, block.value.content)
}

export async function authWNFS () {
  // const state = await wn.initialise({
  //   permissions: {
  //   // Will ask the user permission to store
  //   // your apps data in `private/Apps/Nullsoft/Winamp`
  //     app: {
  //       name: 'Winamp',
  //       creator: 'Nullsoft',
  //     },

  //     // Ask the user permission to additional filesystem paths
  //     fs: {
  //       private: [wn.path.directory('Audio', 'Music')],
  //       public: [wn.path.directory('Audio', 'Mixtapes')],
  //     },
  //   },
  // }).catch(err => {
  //   switch (err) {
  //     case wn.InitialisationError.InsecureContext:
  //       // We need a secure context to do cryptography
  //       // Usually this means we need HTTPS or localhost
  //       throw err

  //     case wn.InitialisationError.UnsupportedBrowser:
  //       // Browser not supported.
  //       // Example: Firefox private mode can't use indexedDB.
  //       throw err
  //   }
  // })

  // console.log('WNFS init:', state)
  // if (!state) throw new Error('no state after init')

  // switch (state.scenario) {
  //   case wn.Scenario.AuthCancelled:
  //   // User was redirected to lobby,
  //   // but cancelled the authorisation
  //     break

  //   case wn.Scenario.AuthSucceeded:
  //   case wn.Scenario.Continuation:
  //   // State:
  //   // state.authenticated    -  Will always be `true` in these scenarios
  //   // state.newUser          -  If the user is new to Fission
  //   // state.throughLobby     -  If the user authenticated through the lobby, or just came back.
  //   // state.username         -  The user's username.
  //   //
  //   // ☞ We can now interact with our file system (more on that later)
  //     console.log('wnfs success', state)
  //     break

  //   case wn.Scenario.NotAuthorised:
  //     await wn.redirectToLobby(state.permissions)
  //     // wn.
  //     break
  // }
}
