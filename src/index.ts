import '@logseq/libs'
import { BlockEntity, IBatchBlock } from '@logseq/libs/dist/LSPlugin.user'
import { LSPluginBaseInfo } from '@logseq/libs/dist/LSPlugin'
import { authWNFS, createIPFSData, loadBlock, loadIPFSData } from './wnfs'

const delay = async (t = 100) => await new Promise(r => setTimeout(r, t))

/**
 * main entry
 * @param baseInfo
 */
function main (baseInfo: LSPluginBaseInfo) {
  let loading = false

  logseq.provideModel({
    async loadReddits () {
      const info = await logseq.App.getUserConfigs()
      console.log('info:', info)
      if (loading) return

      const pageName = 'WNFS test 2'
      const blockTitle = (new Date()).toLocaleString()

      logseq.App.pushState('page', { name: pageName })

      await delay(300)

      loading = true

      try {
        const currentPage = await logseq.Editor.getCurrentPage()
        if (currentPage?.originalName !== pageName) throw new Error('page error')

        const pageBlocksTree = await logseq.Editor.getCurrentPageBlocksTree()
        console.log({ pageBlocksTree })
        // const targetBlock: BlockEntity | null = pageBlocksTree[0]

        // await authWNFS()
        await createIPFSData()

        // await logseq.Editor.updateBlock(targetBlock.uuid, `## 🔖 WNFS 2 - ${blockTitle}`)
      } catch (e) {
        logseq.App.showMsg((e as Error).message ?? JSON.stringify(e), 'warning')
        console.error(e)
      } finally {
        loading = false
      }
    },
  })

  logseq.App.registerUIItem('toolbar', {
    key: 'logseq-reddit',
    template: `
      <a data-on-click="loadReddits"
         class="button">
        <i class="ti ti-brand-reddit"></i>
      </a>
    `,
  })

  logseq.Editor.registerSlashCommand('ce', async (e) => {
    const maybeCid = (await logseq.Editor.getEditingBlockContent()).trim()
    const currentBlock = await logseq.Editor.getCurrentBlock()
    console.log('slash command', e, { maybeCid, currentBlock })
    if (!currentBlock) throw new Error('no current block')

    // const targetBlock = await logseq.Editor.insertBlock(targetBlock.uuid, '🚀 Fetching ...', { before: true })
    // if (!targetBlock) throw new Error('Insert result is null')

    /* const blocks: IBatchBlock[] =  */await loadBlock(maybeCid, currentBlock)
    // await logseq.Editor.insertBatchBlock(targetBlock.uuid, blocks, {
    //   sibling: false,
    // })
  })

  logseq.provideStyle(`
    [data-injected-ui=logseq-reddit-${baseInfo.id}] {
      display: flex;
      align-items: center;
    }
  `)
}

// bootstrap
logseq.ready(main).catch(console.error)
